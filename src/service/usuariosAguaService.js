import Service from './service.js'


const resourse = 'api/usuarios_agua';

export default {
    getUsuariosAgua() {
        let lista = Service.get(resourse);
        return lista;
    },

    create(data){
        return Service.post(`${resourse}/add`, data)
    },

    update(data){
        return Service.put(`${resourse}/update`, data)
    }

}   
