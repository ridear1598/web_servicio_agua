import axios from 'axios';  

const baseUrl = 'http://localhost:3030/';

// const baseUrl = 'https://back-reciclaje.herokuapp.com'
export default axios.create({
    baseURL: baseUrl
});
